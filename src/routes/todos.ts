import { Router } from 'express';
import {
	todoCreate,
	todoDelete,
	todoGet,
	todoPatch,
} from '../controllers/todos';

const router = Router();

router.post('/', todoCreate);
router.get('/', todoGet);
router.patch('/:id', todoPatch);
router.delete('/:id', todoDelete);

export default router;
