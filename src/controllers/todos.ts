import { RequestHandler } from 'express';
import { Todo } from '../models/todo';

const TODOS: Todo[] = [];

export const todoCreate: RequestHandler = (req, res, next) => {
	const text = (<{ text: string }>req.body).text;
	const newTodo: Todo = new Todo(Math.random().toString(), text);

	TODOS.push(newTodo);

	res.json({ message: 'created', createdTodo: newTodo });
};

export const todoGet: RequestHandler = (req, res, next) => {
	res.json({ todos: TODOS });
};

export const todoPatch: RequestHandler<{ id: string }> = (req, res, next) => {
	const todoId = req.params.id;
	const todoText = (<{ text: string }>req.body).text;
	const todoIndex = TODOS.findIndex((todoItem) => todoItem.id === todoId);

	if (todoIndex < 0) {
		throw new Error('todo does not exist');
	}

	TODOS[todoIndex] = new Todo(TODOS[todoIndex].id, todoText);

	res.json({ message: 'updated', updatedTodo: TODOS[todoIndex] });
};

export const todoDelete: RequestHandler = (req, res, next) => {
	const todoId = req.params.id;
	const todoIndex = TODOS.findIndex((todoItem) => todoItem.id === todoId);

	if (todoIndex < 0) {
		throw new Error('todo does not exist');
	}

	const deleted = TODOS.splice(todoIndex, 1);
	res.json({ message: 'deleted', updatedTodo: deleted });
};
