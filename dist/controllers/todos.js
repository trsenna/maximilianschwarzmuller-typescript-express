"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.todoDelete = exports.todoPatch = exports.todoGet = exports.todoCreate = void 0;
const todo_1 = require("../models/todo");
const TODOS = [];
const todoCreate = (req, res, next) => {
    const text = req.body.text;
    const newTodo = new todo_1.Todo(Math.random().toString(), text);
    TODOS.push(newTodo);
    res.json({ message: 'created', createdTodo: newTodo });
};
exports.todoCreate = todoCreate;
const todoGet = (req, res, next) => {
    res.json({ todos: TODOS });
};
exports.todoGet = todoGet;
const todoPatch = (req, res, next) => {
    const todoId = req.params.id;
    const todoText = req.body.text;
    const todoIndex = TODOS.findIndex((todoItem) => todoItem.id === todoId);
    if (todoIndex < 0) {
        throw new Error('todo does not exist');
    }
    TODOS[todoIndex] = new todo_1.Todo(TODOS[todoIndex].id, todoText);
    res.json({ message: 'updated', updatedTodo: TODOS[todoIndex] });
};
exports.todoPatch = todoPatch;
const todoDelete = (req, res, next) => {
    const todoId = req.params.id;
    const todoIndex = TODOS.findIndex((todoItem) => todoItem.id === todoId);
    if (todoIndex < 0) {
        throw new Error('todo does not exist');
    }
    const deleted = TODOS.splice(todoIndex, 1);
    res.json({ message: 'deleted', updatedTodo: deleted });
};
exports.todoDelete = todoDelete;
